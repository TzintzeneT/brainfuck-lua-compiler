#!/usr/bin/env lua
storage = {0} --array to storage all of the values
location = 1 --pointer

--open and read the bf code
BFcode = io.open(arg[1], "r")
BFcode = BFcode:read("*all")

--------------------------------------------------------------------------------

function IHateBrackets(text) --function to create a string with the order of close and open brackets

  local openClose = ""

  for i=1,string.len(text) do --for each char in the text

    if string.sub(text, i, i) == "[" then --if it is open bracket
      openClose = openClose .. "o" --add o
    elseif string.sub(text, i, i) == "]" then --if it is close bracket
      openClose = openClose .. "c" --add o
    end
  end

  return openClose --return the string
end

--------------------------------------------------------------------------------

function FirstBracketsClose(text) --check what is the close bracket of the first bracket in the text and return its number (1st, 2nd etc)

  local brackets = IHateBrackets(text) --get the order of the brackets

  local sum = 0 --how many brackets are open
  local index = 1 --count the number of brackets until the correct close bracket was found
  local enterd = false --make sure that entering the loop is necessary

  while sum ~= 0 or not enterd do --while there is no eqwal opend and closed brackets (which means that the correct close bracket is ahead)
    enterd = true
    if string.sub(brackets, index, index) == "o" then --if it the current bracket is to open
      sum = sum + 1 --add it to the open brackets sum
    else
      sum = sum - 1 --remove one open bracket from the sum
    end
    index = index+1
  end
  return index --return the index where the close bracket is in comparison for the other brackets
end

--------------------------------------------------------------------------------

function BracketMap(text) --map where each bracket close in a text

  local map = {}
  local index = 1

  while index <= string.len(text) do --go over each char in the text

    if string.sub(text, index, index) == "[" then --if it is open bracket

      local closeBrucketIndex = 0
      local remainingText = string.sub(text, index, string.len(text)) --take the remaining text
      local indexAdd = string.len(text) - string.len(remainingText) --calculate the differance between the length of the text

      for i=1, FirstBracketsClose(remainingText)/2 do --get the location of the close bracket in the remaining closing brackets and run that many times
        closeBrucketIndex = string.find(remainingText, "%]", closeBrucketIndex+1) --get the next close bracket
      end
      map[closeBrucketIndex+indexAdd] = index --add to the map the index of the open and close brackets
    end
    index = index + 1 --move to the next char in the text
  end
  return map
end

--------------------------------------------------------------------------------

function RunAction(char) -- function to run specific action

  if char == "+" then --add
    storage[location] = (storage[location] or 0)+1

  elseif char == "-" then --remove
    storage[location] = (storage[location] or 0)-1

  elseif char == ">" then --move right
    location = location+1

  elseif char == "<" then --move left
    location = location-1

  elseif char == "." then --print
    io.write(string.char(storage[location]))

  elseif char == "," then --input
    storage[location] = string.byte(string.sub(io.read(), 1, 1))

  end
  --print(table.concat(storage, "|"))
  --print("location: " .. location)
end

--------------------------------------------------------------------------------

function RunOver(code) --function to run over all of the actions

  local index = 1
  local brackets = BracketMap(code) --get a map with where each bracket open and close

  while index <= string.len(code) do --run over each action

    action = string.sub(code, index, index) --get the current action

    if action == "]" and storage[location] ~= 0 then --if it is the end of a loop and the exit conditions isn't true
      index = brackets[index] --move to the start of the loop
    else
      RunAction(action) --run the current action
    end
    --print(index)
    index = index + 1 --move to the next action
  end
end

--------------------------------------------------------------------------------

--Run over the BrainF**k code
if pcall(RunOver, BFcode) then
  print()
  print("code executed successfully")
else
  print()
  print("error")
end

--print(table.concat(storage, "|"))
